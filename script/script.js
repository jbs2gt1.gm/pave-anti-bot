const pavéMaxLigne = 6;
const pavéMinLigne = 4;

const pavéMaxColonne = 6;
const pavéMinColonne = 4;

const résultatConnexion = "#result";

const fennConnect = "#dialogConnect";
const fennInscription = "#dialogInscription"

var text = "";

$(function(){
    $(résultatConnexion).val("");//on vide le texte quand la page charge

    setTab();
    changeNumber();

    $( fennConnect ).dialog({
        autoOpen: false
    });

    $('#PaveNum tr th button').click(function() {
        text += $(this).text();
        $(résultatConnexion).val(text);
        changeNumber ();
    });

    //ouverture de la fenetre de connection 
    $("#Connexion").click(function() {
        $(fennConnect).dialog("open");
    });

    $("#Corriger").click(function(){
        text = text.slice(0, -1);
        $(résultatConnexion).val(text);
    });

    $("#Recharger").click(function() {
        text ="";
        $(résultatConnexion).val(text);
    });
});

/*
Atribution aléatoire des nombres aux touches du pavé
*/
function changeNumber () {
    boutons = $('#PaveNum tr th button');
    var activate = [];
    var tab = ["0","1","2","3","4","5","6","7","8","9","*","#"];

    boutons.text("");
    //chois des boutons utilisé
    for(let i=0; i< tab.length; i++){
        let random = Math.floor(Math.random() * boutons.length);//numéro aléatoire du bouton
        activate.push(boutons[random]);//ajout du bouton à activate
        boutons.splice(random, 1);//suppression de la liste boutons pour qu'il ne soit pas choisit deux fois
    }

    $(activate).each(function () {
        var indexAlea = Math.floor(Math.random() * tab.length);
        $(this).text(tab[indexAlea]);
        tab.splice(indexAlea, 1);
    });
    
}

/*
Configuration aléatoire de la taille du pavé numérique
*/
function setTab() {
    var ligne = Math.floor(Math.random() * (pavéMaxLigne-pavéMinLigne)) + pavéMinLigne;
    var colonne = Math.floor(Math.random() * (pavéMaxColonne - pavéMinColonne)) + pavéMinColonne;

    for(let i = 0; i < ligne; i++){
        $('#PaveNum').append("<tr></tr>");
    }

    $('#PaveNum tr').each(function () {
        for(let i = 0; i < colonne; i++){
            $(this).append('<th><button type="button" class="numTouch"></button></th>');
        }
    });
    
}